#Per eseguire il codice è necessario lanciare da console assignment_bayesian.py

import random

#Lista di adiacenza
#Ordine topologico (RIGHE) W,H,A,J
#Dipendenze (COLONNE) W,H,A,J
adj_matrix = [[0,0,0,0],[0,0,0,0],[1,0,0,0],[1,1,1,0]]

#Definizione delle condition probabiliy table per ogni nodo
cpt_h = 0.2
cpt_w = 0.05
cpt_a = {   1: 0.1,
            0: 0.3}
cpt_j = dict()
cpt_j = {   "111": 0.95,
            "110": 0.95,
            "101": 0.95,
            "100": 0.95,
            "011": 0.5,
            "010": 0.3,
            "001": 0.6,
            "000": 0.1}

#Funzione utilizzata per generare i valori casuali delle variabili senza evidenza
#Prende in input la quantità di campioni che vogliono essere generati
def generate_random_values(size):
    nume = 0
    deno = 0
    values = dict()
    #Definisco una struttura dati a dizionario per conservare i valori randomici generati per i nodi
    #Inizialmente tutti a zero. Il valore di A non verrà generato poichè possiedo evidenza
    values = {  "h": 0,
                "w": 0,
                "j": 0}

    #Genero valori casuali tra 0 e 1 per le variabili senza evidenza
    for i in range(0,size):
        values["h"] = random.random()
        values["w"] = random.random()
        values["j"] = random.random()
        (num,den) = generate_samples(values)
        #Effettuo la sommatoria degli n samples
        nume = nume + num
        deno = deno + den

    #Ritorno il valore finale di proabilità da stimare
    return nume/deno

#Funzione utilizzata per definire i valori della variabili senza evidenza una volta generati i valori casuali
#Prende in input il dizionario precedentemente specificato, contenente i valori casuali generati
def generate_samples(values):
    #Definisco un'altra struttura dati a dizionario per salvare i valori di verità delle variabili
    sample = dict()
    sample = {  "h": 0,
                "w": 0,
                "a": 1,
                "j": 0}

    #Valuto i valori dei vari nodi padre
    if(values["w"] < cpt_w):
        sample["w"] = 1
    else:
        sample["w"] = 0

    if(values["h"] < cpt_h):
        sample["h"] = 1
    else:
        sample["h"] = 0

    #Salvo lo stato delle variabili che infulenzano J in una stringa
    state_h_w_a = ""
    state_h_w_a = state_h_w_a + str(sample["h"])
    state_h_w_a = state_h_w_a + str(sample["w"])
    state_h_w_a = state_h_w_a + str(sample["a"])

    #Attraverso accesso a chiave sulla cpt di J calcolo il valore di verità di J influenzato dagli altri nodi
    if(values["j"] < cpt_j[state_h_w_a]):
        sample["j"] = 1
    else:
        sample["j"] = 0

    (num,den) = likelihood_weghthing(sample)
    return (num,den)

#Funzione utilizzata per effettuare il calolo della likelihood pesata
#Prende in input il dizionario contenentei valori di verità delle variabili
def likelihood_weghthing (sample):
    #Inizializzo numeratore e denominatore da utilizzare per il calcolo della sommatoria
    num = 0
    den = 0
    if(sample["w"] == 1):
        weight = cpt_a[1]
    else:
        weight = cpt_a[0]

    if(sample["j"] == 1):
        num = num + weight

    den = weight

    #Ritorno il valore del numeratore e denominatore della formula
    return (num,den)


#Stampo a console il valore finale di probabilità da stimare generando un po' di esempi
#La probabilità ricade più o meno nell'intervallo 0,64 e 0,68
for i in range(0,10):
    print(generate_random_values(1000))
